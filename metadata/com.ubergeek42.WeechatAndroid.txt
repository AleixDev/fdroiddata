Categories:Internet
License:Apache2
Web Site:https://github.com/ubergeek42/weechat-android/wiki
Source Code:https://github.com/ubergeek42/weechat-android
Issue Tracker:https://github.com/ubergeek42/weechat-android/issues

Auto Name:weechat
Summary:Internet relay chat
Description:
Relay Client for the Weechat IRC client. It allows you to connect to your phone
to your Weechat client and read/reply to your messages while away from your
computer.
This is the '''DEVELOPMENT''' version of this client. It may contain bugs or
features that are not fully implemented.
This is '''NOT''' a fully featured IRC client. It requires weechat running on a
server to connect to.

[https://github.com/ubergeek42/weechat-android/raw/master/Readme.md Changelog]
.

# pom is messy and assumes signing
Repo Type:git
Repo:https://github.com/ubergeek42/weechat-android.git

Build:0.8-dev-b4,4
    commit=v0.08-dev-b4
    subdir=weechat-android
    extlibs=sl4j/slf4j-android-1.6.1-RC1.jar,acra/acra-4.4.0.jar
    srclibs=ActionBarSherlock@4.1.0,ViewPagerIndicator@2.4.1
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        sed -i 's@\(android.library.reference.2=\).*@\1$$ViewPagerIndicator$$@' project.properties && \
        mvn clean package -f ../weechat-relay/pom.xml && \
        mkdir -p libs/ && \
        mv ../weechat-relay/target/weechat-relay-0.8-SNAPSHOT.jar libs/

Build:0.8-dev-b5,5
    commit=b484867803
    subdir=weechat-android
    extlibs=sl4j/slf4j-android-1.6.1-RC1.jar,acra/acra-4.4.0.jar
    srclibs=ActionBarSherlock@4.1.0,ViewPagerIndicator@2.4.1
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        sed -i 's@\(android.library.reference.2=\).*@\1$$ViewPagerIndicator$$@' project.properties && \
        mvn clean package -f ../weechat-relay/pom.xml && \
        mkdir -p libs/ && \
        mv ../weechat-relay/target/weechat-relay-0.8-SNAPSHOT.jar libs/

Auto Update Mode:None
# Tags lag behind, master often has pre-releases
Update Check Mode:None
Current Version:0.8-dev-b5
Current Version Code:5

